import type { Config } from 'tailwindcss';

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      colors: {
        'background': '#FBFBFB',
        'dark': '#272B30',
        'grey4': '#979DA4',
        'grey3': '#CFCFCF',
        'grey2': '#E5E5E5',
        'grey1': '#F5F5F5',
        'green1': '#5EEAAF',
        'green2': '#17A684',
        red: '#FB5757',
      },
      boxShadow: {
        'light': '0 0 10px 0 rgba(0, 0, 0, 0.07)',
      },
      fontSize: {
        '2xs': ['10px', '12px'],
        '3xs': ['8px', '10px'],
      },
      borderRadius: {
        '10px': '10px',
      },
      transitionDelay: {
        '25': '25ms',
        '15': '15ms',
      }
    },
  },
  plugins: [],
};
export default config;
