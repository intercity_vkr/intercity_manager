import {Route} from "./route";
import {Company, fromAPICompany} from "@/entities/company";
import {CarEntity, fromAPICar} from "@/entities/car";
import {TripPassengerConfirmed} from "@/entities/trip-passenger-confirmed";
import {TripPassengerRequested} from "@/entities/trip-passenger-requested";
import {fromAPIRoute} from "@/entities/route";
import {fromAPIPassenger, Passenger} from "@/entities/passenger";
import {fromAPIPlace} from "@/entities/place";
import {number} from "prop-types";

export type Trip = {
    tripID: string;
    route: Route;
    price: number
    reversed: boolean;
    company: Company;
    startTime: string;
    endTime: string;
    car: CarEntity;
    // confirmedPassengers: TripPassengerConfirmed[];
    // requestPassengers: TripPassengerRequested[];
    tripSeatingChart: string;
    tripPassengers: Passenger[]
}

export function fromAPITrip(apiTrip: any): Trip {
    return {
        tripID: apiTrip.trip_id,
        price: apiTrip.full_price,
        route: fromAPIRoute(apiTrip.route),
        reversed: apiTrip.reversed,
        company: fromAPICompany(apiTrip.company),
        startTime: apiTrip.start_time,
        endTime: apiTrip.end_time,
        car: fromAPICar(apiTrip.car),
        tripSeatingChart: '',
        tripPassengers: []
        // confirmedPassengers: apiTrip.confirmed_passengers.map((apiConfirmedPassenger: any) => {
        //     return {
        //         passenger: fromAPIPassenger(apiConfirmedPassenger.passenger),
        //         seatNumber: apiConfirmedPassenger.seat_number,
        //         price: apiConfirmedPassenger.price,
        //         endPlace: fromAPIPlace(apiConfirmedPassenger.end_place)
        //     };
        // }),
        // requestPassengers: apiTrip.request_passengers.map((apiRequestedPassenger: any) => {
        //     return {
        //         passenger: fromAPIPassenger(apiRequestedPassenger.passenger),
        //         endPlace: fromAPIPlace(apiRequestedPassenger.end_place)
        //     };
        // })
    };
}

export type CreateTripInput = {
    routeID: string;
    reversed: boolean;
    carID: string;
    startTime: string;
    endTime: string;
    tripSeatingChart: string
    price: number
}

export function toAPICreateTripRequest(input: CreateTripInput): any {
    return {
        route_id: input.routeID,
        reversed: input.reversed,
        car_id: input.carID,
        start_time: input.startTime,
        end_time: input.endTime,
        custom_seating_chart: input.tripSeatingChart,
        price: input.price
    };
}

export const emptyTrip: Trip = {
    tripID: "",
    tripSeatingChart: "",
    price: 0,
    route: {
        routeID: "",
        name: "",
        description: "",
        startPlace: {
            placeID: "",
            name: "",
            point: {
                latitude: 0,
                longitude: 0
            }
        },
        endPlace: {
            placeID: "",
            name: "",
            point: {
                latitude: 0,
                longitude: 0
            }
        },
        stops: []
    },
    reversed: false,
    company: {
        companyID: "",
        name: "",
        address: ""
    },
    startTime: "",
    endTime: "",
    car: {
        carID: "",
        mark: "",
        model: "",
        gosnomer: "",
        color: "",
        seatingChart: "",
        company: {
            companyID: "",
            name: "",
            address: ""
        }
    },
    tripPassengers: []
    // confirmedPassengers: [],
    // requestPassengers: []
};
