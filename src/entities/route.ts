import {fromAPIPlace, Place} from "./place";

export type Route = {
    routeID: string;
    name: string;
    description: string;
    startPlace: Place;
    endPlace: Place;
    stops: Place[];
}

export function fromAPIRoute(apiRoute: any): Route {
    return {
        routeID: apiRoute.route_id,
        name: apiRoute.name,
        description: apiRoute.description,
        startPlace: fromAPIPlace(apiRoute.start_place),
        endPlace: fromAPIPlace(apiRoute.end_place),
        stops: apiRoute.stops.map((apiStop: any) => fromAPIPlace(apiStop))
    };
}

export type CreateRouteInput = {
    name: string;
    description: string;
    startPlaceID: string;
    endPlaceID: string;
    stopsIDs: string[];
}

export function toAPICreateRouteRequest(input: CreateRouteInput): any {
    return {
        name: input.name,
        description: input.description,
        start_place_id: input.startPlaceID,
        end_place_id: input.endPlaceID,
        stops_ids: input.stopsIDs
    };
}

export type RouteFilterRequest = {
    name?: string;
    start_place_id?: string;
    end_place_id?: string;
};
