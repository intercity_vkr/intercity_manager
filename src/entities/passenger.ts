export type Passenger = {
    passengerID: string;
    firstName: string;
    lastName: string;
    phone: string;
}

export function fromAPIPassenger(apiPassenger: any): Passenger {
    return {
        passengerID: apiPassenger.passenger_id,
        firstName: apiPassenger.first_name,
        lastName: apiPassenger.last_name,
        phone: apiPassenger.phone
    };
}