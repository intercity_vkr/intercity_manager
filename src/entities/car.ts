import {Company} from "@/entities/company";

export type CarEntity = {
    carID: string;
    mark: string;
    model: string;
    gosnomer: string;
    color: string;
    seatingChart: string;
    company: Company;
}

export type CreateCarEntity = {
    mark: string;
    model: string;
    gosnomer: string;
    color: string;
    seatingChart: string;
}

export type UpdateCarEntity = {
    carID: string;
    mark: string;
    model: string;
    gosnomer: string;
    color: string;
    seatingChart: string;
}

export const emptyCar: CarEntity = {
    carID: "",
    mark: "",
    model: "",
    gosnomer: "",
    color: "",
    seatingChart: "",
    company: {
        companyID: "",
        name: "",
        address: ""
    }
};