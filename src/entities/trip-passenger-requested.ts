import {Passenger} from "@/entities/passenger";
import {Place} from "@/entities/place";

export type TripPassengerRequested = {
    passenger: Passenger
    endPlace: Place
}