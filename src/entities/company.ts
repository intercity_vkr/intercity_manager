export type Company = {
    companyID: string;
    name: string;
    address: string;
}

export function fromAPICompany(apiCompany: any): Company {
    return {
        companyID: apiCompany.company_id,
        name: apiCompany.name,
        address: apiCompany.address
    };
}