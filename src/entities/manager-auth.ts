export type ManagerAuthInitResponseEntity = {
    managerID: string
}

export type ManagerAuthCodeResponseEntity = {
    accessToken: string
    refreshToken: string
    responseCode: string
}

