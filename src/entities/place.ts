export type Place = {
    placeID: string;
    name: string;
    point: {
        latitude: number;
        longitude: number;
    };
}

export function fromAPIPlace(apiPlace: any): Place {
    return {
        placeID: apiPlace.place_id,
        name: apiPlace.name,
        point: {
            latitude: apiPlace.point.latitude,
            longitude: apiPlace.point.longitude
        }
    };
}

export type CreatePlaceInput = {
    name: string;
    latitude: number;
    longitude: number;
}

export function toAPICreatePlaceRequest(input: CreatePlaceInput): any {
    return {
        name: input.name,
        latitude: input.latitude,
        longitude: input.longitude
    };
}