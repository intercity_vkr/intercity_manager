import {Passenger} from "@/entities/passenger";
import {Place} from "@/entities/place";

export type TripPassengerConfirmed = {
    passenger: Passenger
    seatNumber: number
    price: number
    endPlace: Place
}