import Cookies from 'universal-cookie';
import axios, {isAxiosError} from "axios";
import AppConfig from "@/app-config";
import {ECodeResult, EInitAuthResult} from "@/services/manager-auth.service/manager-auth.results";
import {getTokenFromCookies} from "@/services/common";
import IManagerAuthRepo from "@/services/manager-auth.service/manager-auth.repo";
import {ManagerAuthCodeResponseEntity, ManagerAuthInitResponseEntity} from "@/entities/manager-auth";
import {ManagerAuthMapper} from "@/data/API/manager-auth/manager-auth.mapper";

class ManagerAuthAPI implements IManagerAuthRepo {
    apiUrl = AppConfig.apiUrl + '/manager/auth/'

    async initAuth(phone: string): Promise<ManagerAuthInitResponseEntity> {
        try {
            const response = await axios.post(this.apiUrl + 'init', {phone})
            return ManagerAuthMapper.fromAPIInitAuth(response.data)
        } catch (error) {
            console.error('Error while initializing auth:', error);
            throw error;
        }
    }

    async code(managerID: string, code: string): Promise<ManagerAuthCodeResponseEntity> {
        try {
            const response = await axios.post(this.apiUrl + 'code', {manager_id: managerID, code: code})
            return ManagerAuthMapper.fromAPICode(response.data, response.status.toString())
        } catch (error) {
            console.error('Error while sending auth code:', error);
            throw error;
        }
    }

}