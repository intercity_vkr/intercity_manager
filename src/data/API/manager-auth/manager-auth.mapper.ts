import {ManagerAuthCodeResponseEntity, ManagerAuthInitResponseEntity} from "@/entities/manager-auth";

export const ManagerAuthMapper = {
    fromAPIInitAuth(apiAuthInitResponse: any): ManagerAuthInitResponseEntity {
        return {
            managerID: apiAuthInitResponse.manager_id,
        }
    },

    fromAPICode(apiCodeResponse: any, responseCode: string): ManagerAuthCodeResponseEntity {
        return {
            accessToken: apiCodeResponse.access_token,
            refreshToken: apiCodeResponse.refresh_token,
            responseCode: responseCode,
        }
    }
}