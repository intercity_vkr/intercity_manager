import {CarEntity, CreateCarEntity, UpdateCarEntity} from '@/entities/car';
import axios, { AxiosResponse } from 'axios';
import appConfig from "@/app-config";
import {CarMapper} from "@/data/API/car/car.mapper";
import ICarRepo from "@/services/car.service/car.repo";

class CarAPI implements ICarRepo {
    API_URL = `${appConfig.apiUrl}/car`;

    async getAll(token: string): Promise<CarEntity[]> {
        try {
            const config = { headers: { Authorization: 'Bearer ' + token } }
            const response: AxiosResponse = await axios.get(`${this.API_URL}/list`, config);
            return response.data.map((apiCar: any) => CarMapper.fromAPI(apiCar));
        } catch (error) {
            console.error('Error while fetching cars:', error);
            throw error;
        }
    }

    async getById(token: string, carID: string): Promise<CarEntity|undefined> {
        try {
            const config = { headers: { Authorization: 'Bearer ' + token } }
            const response: AxiosResponse = await axios.get(`${this.API_URL}/list`, config);
            return response.data.map((apiCar: any) => CarMapper.fromAPI(apiCar)).find((car: CarEntity) => car.carID == carID);
        } catch (error) {
            console.error(`Error while fetching car with ID ${carID}:`, error);
            throw error;
        }
    }

    async create(token: string, createCarEntity: CreateCarEntity): Promise<CarEntity> {
        try {
            const config = { headers: { Authorization: 'Bearer ' + token } }
            const response: AxiosResponse<CarEntity> = await axios.post(`${this.API_URL}/create`, CarMapper.toAPICreate(createCarEntity), config);
            return CarMapper.toAPICreate(response.data);
        } catch (error) {
            console.error('Error while creating car:', error);
            throw error;
        }
    }

    async update(token: string, carID: string, updateCarEntity: UpdateCarEntity): Promise<CarEntity> {
        try {
            const config = { headers: { Authorization: 'Bearer ' + token } }
            const response: AxiosResponse<CarEntity> = await axios.put(`${this.API_URL}/update`, CarMapper.toAPIUpdate(updateCarEntity), config);
            return response.data;
        } catch (error) {
            console.error(`Error while updating car with ID ${carID}:`, error);
            throw error;
        }
    }
}
