import {fromAPICompany} from "@/entities/company";
import {CarEntity, CreateCarEntity, UpdateCarEntity} from "@/entities/car";

export const CarMapper = {
    fromAPI(apiCar: any): CarEntity {
        return {
            carID: apiCar.car_id,
            mark: apiCar.mark,
            model: apiCar.model,
            gosnomer: apiCar.gosnomer,
            color: apiCar.color,
            seatingChart: apiCar.seating_chart,
            company: fromAPICompany(apiCar.company)
        };
    },

    toAPICreate(createCarEntity: CreateCarEntity): any {
        return {
            mark: createCarEntity.mark,
            model: createCarEntity.model,
            gosnomer: createCarEntity.gosnomer,
            color: createCarEntity.color,
            seating_chart: createCarEntity.seatingChart
        }
    },

    toAPIUpdate(updateCarEntity: UpdateCarEntity): any {
        return {
            car_id: updateCarEntity.carID,
            mark: updateCarEntity.mark,
            model: updateCarEntity.model,
            gosnomer: updateCarEntity.gosnomer,
            color: updateCarEntity.color,
            seating_chart: updateCarEntity.seatingChart
        }
    }


}