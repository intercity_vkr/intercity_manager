'use client'

import Image from "next/image";

interface IconButtonProps {
    iconSrc: string,
    width?: number,
    height?: number,
}
const IconButton = ({iconSrc, width = 18, height = 18}: IconButtonProps) => {
    return (
        <Image src={iconSrc} alt={''} width={width} height={height}/>
    );
};

export default IconButton;