import React, {useEffect, useState} from 'react';
import {Stop} from "@/entities/stop";
import ListItem from "@/components/ui/list-item/ListItem";
import styles from "./page.module.scss";
import Checkbox from "@/components/ui/checkbox/Checkbox";

interface StopsPickerProps {
    stops: Stop[]
    selectedStops: Stop[]
    onChange: (value: Stop[]) => void
}

const StopsPicker = (props:StopsPickerProps) => {
    const [stops, setStops] = useState<Stop[]>(props.stops)
    const [selectedStops, setSelectedStops] = useState<Stop[]>(props.selectedStops)

    useEffect(() => {
        setStops(props.stops)
        setSelectedStops(props.selectedStops)
    }, [props.selectedStops])

    const addStop = (stop: Stop) => {
        setSelectedStops([...selectedStops, stop])
        props.onChange([...selectedStops, stop])
    }

    const deleteStop = (stop: Stop) => {
        const stops = selectedStops.filter((val) => JSON.stringify(val) != JSON.stringify(stop))
        setSelectedStops(stops)
        props.onChange(stops)
    }

    const handleCheckboxClick = (stop: Stop, checked: boolean) => {
        if (!checked) {
            deleteStop(stop)
        } else {
            addStop(stop)
        }
    }

    return (
        <div>
            <ListItem
                text={'Указать на карте'}
                textClassName={styles.greenText}
            />
            <hr/>
            {stops.map((stop, idx) => (
                <div key={stop.id}>
                    <ListItem
                        text={stop.name}
                        right={<Checkbox
                            key={stop.id}
                            checked={selectedStops.find(val => JSON.stringify(val) == JSON.stringify(stop)) != undefined}
                            onChange={(e) => {
                                handleCheckboxClick(stop, e.target.checked)
                            }}
                        />}
                    />
                    {idx + 1 < props.stops.length ? <hr/> : <div></div>}
                </div>
            ))}
        </div>
    );
};

export default StopsPicker;