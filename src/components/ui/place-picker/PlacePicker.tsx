"use client"

import React, {useEffect, useState} from 'react';
import styles from "./page.module.scss";
import Input from "@/components/ui/input/Input";
import ListItem from "@/components/ui/list-item/ListItem";
import Icon from "@/components/ui/icon/Icon";

interface PlacePickerProps {
    value: string
    onChange: (value: string) => void
}

const PlacePicker = (props: PlacePickerProps) => {
    const [place, setPlace] = useState(props.value)

    useEffect(() => {
        setPlace(props.value)
    }, [props.value])

    return (
        <>
            <Input
                className={styles.input}
                placeholder={'Укажите адрес'}
                value={place}
                onChange={e => {
                    props.onChange(e.target.value)
                    setPlace(e.target.value)
                }}
            />
            <ListItem
                text={'Указать на карте'}
                left={<Icon src={'/icons/pin.svg'}/>}
                right={<Icon src={'/icons/chevron-right-sm.svg'} width={5} height={10}/>}
            />
            <hr/>
            <div onClick={() => {
                props.onChange('Екатеринбург')
                setPlace('Екатеринбург')
            }}>
                <ListItem
                    text={'Екатеринбург'}
                    left={<Icon src={'/icons/clock.svg'}/>}
                    right={<Icon src={'/icons/chevron-right-sm.svg'} width={5} height={10}/>}
                />
            </div>
            <hr/>
            <div onClick={() => {
                props.onChange('Пионерский')
                setPlace('Пионерский')
            }}>
                <ListItem
                    text={'Пионерский'}
                    left={<Icon src={'/icons/clock.svg'}/>}
                    right={<Icon src={'/icons/chevron-right-sm.svg'} width={5} height={10}/>}
                />
            </div>
        </>
    );
};

export default PlacePicker;