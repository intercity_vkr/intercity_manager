import Image from "next/image";

export interface IconProps {
    src: string;
    width?: number;
    height?: number;
    className?: string;
}
const Icon = (props: IconProps) => {
    return (
        <Image
            className={props.className}
            src={props.src}
            alt={''}
            width={(props.width && props.height) ? props.width : 0}
            height={(props.width && props.height) ? props.height : 0}
            style={(props.width && props.height) ? {} : {width: 16, height: "auto"}}
        />
    );
};

export default Icon;