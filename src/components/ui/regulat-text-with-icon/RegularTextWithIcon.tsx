import styles from './RegularTextWithIcon.module.scss'
import Icon from "@/components/ui/icon/Icon";
import Image from "next/image";

interface RegularTextWithIconProps {
    text: string;
    iconSrc: string;
    iconWidth?: number;
    iconHeight?: number;
    className?: string;
    textClassName?: string;
}
const RegularTextWithIcon = (props: RegularTextWithIconProps) => {
    return (
        <div className={`${styles.iconAndText} ${props.className}`}>
            <Icon src={props.iconSrc} width={props.iconWidth} height={props.iconHeight}/>
            <p className={props.textClassName}>{props.text}</p>
        </div>
    );
};

export default RegularTextWithIcon;