import styles from "./TripItem.module.scss";
import Image from "next/image";
import {ITripItemProps} from "@/components/ui/trip-item/TripItemProps.interface";
import dayjs from "dayjs";

const TripItem = (props: ITripItemProps) => {
    const startDate = dayjs(props.trip.startTime)
    const endDate = dayjs(props.trip.endTime)

    return (
        <div className={styles.card}>
            <div className={styles.routeAndPrice}>
                <div className={styles.route}>{props.trip.route.startPlace.name} – {props.trip.route.endPlace.name}</div>
                <div>{props.trip.price.toString()} ₽</div>
            </div>
            <div className={styles.datetime}>
                <div className={styles.time}>{startDate.format('HH:mm')}</div>
                <Image className={styles.arrow} src={'/decoration/arrow-right.svg'} alt={''}
                       width={23.5} height={8}/>
                <div className={styles.time}>{endDate.format('HH:mm')}</div>
                <div className={styles.date1}>{startDate.date() + ' ' + startDate.format('MMMM')}</div>
                <div className={styles.date2}>{endDate.date() + ' ' + endDate.format('MMMM')}</div>
            </div>
            <div className={styles.durationAndCarrier}>
                <div className={styles.text}>
                    {4} ч в пути
                </div>
                <div className={styles.text}>Перевозчик: <span
                    className={styles.highlight}>{props.trip.company.name}</span></div>
            </div>
            <div className={styles.seatsAndVehicle}>
                <div className={styles.seats}>{1}/{3} места свободно</div>
                <div className={styles.vehicle}>
                    <Image src={'icons/car.svg'} alt={''} width={15.3} height={12.2}/>
                    <div className={styles.text}>{props.trip.car.mark + ' ' + props.trip.car.model}</div>
                </div>
            </div>
        </div>
    );
};

export default TripItem;