import {Trip} from "@/entities/trip";

export interface ITripItemProps {
    trip: Trip
}