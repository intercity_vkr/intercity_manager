import React from "react";
import styles from "./ListItem.module.scss";

interface ListItemProps {
    text: string;
    left?: React.ReactNode;
    right?: React.ReactNode;
    textClassName?: string;
}

const ListItem = (props: ListItemProps) => {
    return (
        <div className={styles.item}>
            <div className={styles.left}>
                {props.left ? props.left : <></>}
                <p className={props.textClassName}>{props.text}</p>
            </div>
            <div className={styles.right}>{props.right ? props.right : <></>}</div>
        </div>
    );
};

export default ListItem;