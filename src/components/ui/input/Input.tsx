import styles from './Input.module.scss'
import {ChangeEventHandler} from "react";

interface InputProps {
    placeholder: string;
    className?: string;
    value?: string | number;
    onChange?: ChangeEventHandler<HTMLInputElement>;
    type?: string;
}

const Input = (props: InputProps) => {
    return (
        <div>
            <input
                className={`${styles.input} ${props.className}`}
                placeholder={props.placeholder}
                value={props.value}
                onChange={props.onChange}
                type={props.type ?? ''}
            />
        </div>
    );
};

export default Input;