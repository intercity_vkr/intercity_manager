import styles from './Caption.module.scss'
import {ReactNode} from "react";

interface CaptionProps {
    captionText: string;
    className?: string;
}
const Caption = (props: CaptionProps) => {
    return (
            <p className={`${props.className} ${styles.caption}`}>{props.captionText}</p>
    );
};

export default Caption;