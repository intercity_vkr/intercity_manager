import React from 'react';
import styles from './Tabbar.module.scss'
import IconButton from "@/components/ui/icon-button/IconButton";
import Link from "next/link";

const Tabbar = () => {
    return (
        <div className={styles.navbar}>
            <div className={styles.icons}>
                <Link href={'/trips'}>
                    <IconButton iconSrc={'/icons/home.svg'} width={24} height={24}/>
                </Link>
                <Link href={'/reservations'}>
                    <IconButton iconSrc={'/icons/people.svg'} width={24} height={24}/>
                </Link>
                <Link href={'/vehicles'}>
                    <IconButton iconSrc={'/icons/car2.svg'} width={28} height={28}/>
                </Link>
                <Link href={'/menu'}>
                    <IconButton iconSrc={'/icons/menu.svg'} width={22} height={22}/>
                </Link>
            </div>
        </div>
    );
};

export default Tabbar;