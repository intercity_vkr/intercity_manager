'use client';

import Image from "next/image";
import styles from "./Navbar.module.scss";
import {useRouter} from "next/navigation";

interface NavbarProps {
    title: String,
    showBackButton?: boolean,
    backButtonAction?: Function,
}

const Navbar = (props: NavbarProps) => {
    const router = useRouter();

    return (
        <div className={styles.navbar}>
            {
                props.showBackButton ?
                    <Image src={'/icons/chevron-left.svg'} alt={''} width={10.14} height={18} onClick={
                        () => props.backButtonAction ? props.backButtonAction() : router.back()
                    }/>
                :
                    <div></div>
            }
            <div className={styles.title}>{ props.title }</div>
            <div></div>
        </div>
    );
};

export default Navbar;