import Heading from "@/components/ui/heading/Heading";
import styles from "./page.module.scss";
import ListItem from "@/components/ui/list-item/ListItem";
import {CarService} from "@/services/car.service/car.service";
import {useEffect, useState} from "react";
import {emptyCar, CarEntity} from "@/entities/car";
import Checkbox from "@/components/ui/checkbox/Checkbox";

interface VehiclePickerProps {
    selectedVehicle: CarEntity
    onChange: (value: CarEntity) => void
}

const VehiclePicker = (props: VehiclePickerProps) => {
    const [vehicles, setVehicles] = useState<CarEntity[]>([])
    const [selectedVehicle, setSelectedVehicle] = useState<CarEntity|undefined>(undefined)

    useEffect(() => {
        CarService.getAll().then((res) => setVehicles(res))
        setSelectedVehicle(props.selectedVehicle)
    }, [props.selectedVehicle])

    const handleCheckboxClick = (vehicle: CarEntity, checked: boolean) => {
        if (!checked) {
            setSelectedVehicle(undefined)
            props.onChange(emptyCar)
        } else {
            setSelectedVehicle(vehicle)
            props.onChange(vehicle)
        }
    }

    return (
        <div>
            <Heading className={styles.heading} text={'Выберите транспортное средство'}/>
            {vehicles.map((vehicle, idx) => (
                <div key={vehicle.carID}>
                    <ListItem
                        text={vehicle.mark + ' ' + vehicle.model + ' ' + vehicle.color}
                        right={
                            <Checkbox
                                checked={JSON.stringify(selectedVehicle) == JSON.stringify(vehicle)}
                                onChange={(event) => {
                                    console.log(JSON.stringify(vehicle))
                                    console.log(JSON.stringify(selectedVehicle))
                                    handleCheckboxClick(vehicle, event.target.checked)
                                }}
                            />
                        }
                    />
                    {idx + 1 != vehicles.length ? <hr/> : <div></div> }
                </div>
            ))}
        </div>
    );
};

export default VehiclePicker