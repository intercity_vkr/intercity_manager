import { useState } from "react"
import DatePicker, { Calendar } from "react-multi-date-picker"
import TimePicker from "react-multi-date-picker/plugins/time_picker";

export default function MyDatePicker() {
    const [value, setValue] = useState(new Date())

    return (
        <DatePicker
            value={value}
            format="MM/DD/YYYY HH:mm"
            plugins={[
                <TimePicker position="bottom" />
            ]}
            portal={false}
        />
    )
}