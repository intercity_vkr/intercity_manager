'use client';

import styles from './Button.module.scss'

interface ButtonProps {
    text: string;
    className?: string;
    textClassName?: string;
    onClick: Function;
    disabled?: boolean
}

const Button = (props: ButtonProps) => {
    return (
        <div
            className={`${props.disabled ? styles.buttonDisabled : styles.button} ${props.className}`}
            onClick={() => {
                if (!props.disabled) {
                    props.onClick()
                }
            }}>
            <p className={props.textClassName}>{props.text}</p>
        </div>
    );
};

export default Button;