import styles from './Checkbox.module.scss'
import {ChangeEventHandler} from "react";

interface ICheckboxProps {
    checked: boolean
    disabled?: boolean
    onChange?: ChangeEventHandler<HTMLInputElement>

}

const Checkbox = (props: ICheckboxProps) => {
    return (
            <input
                className={styles.checkbox}
                type={'checkbox'}
                checked={props.checked}
                disabled={props.disabled}
                onChange={props.onChange}
            />
    );
};

export default Checkbox;