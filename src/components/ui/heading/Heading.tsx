import styles from './Heading.module.scss'
import {ReactNode} from "react";

interface HeadingProps {
    text: string;
    className?: string;
}
const Heading = (props: HeadingProps) => {
    return (
        <div className={`${styles.heading} ${props.className}`}>{props.text}</div>
    );
};

export default Heading;