'use client';

import React, {useState} from 'react';
import {v4 as uuid} from "uuid";
import styles from './SeatPicker.module.scss'

interface ISeatPickerProps {
    seatsScheme: string
    onSeatSelected: (newSeatScheme: string) => void
}

// 'BSV!OVO'
// Свободно - V
// Занято - O
// Недоступно - B
// Пространство - S


const SeatPicker = (props: ISeatPickerProps) => {
    const [selectedSeat, setSelectedSeat] = useState(0)

    const rowLength = () => props.seatsScheme.split('!')[0].length

    const seatNumber = (rowIdx: number, seatIdx: number): number => (rowIdx) * rowLength() + seatIdx + 1

    const getSeatsScheme = (rowIdx: number, seatIdx: number) => {
        const idx = rowIdx * rowLength() + seatIdx + rowIdx
        return props.seatsScheme.substring(0, idx) + 'O' + props.seatsScheme.substring(idx + 1, props.seatsScheme.length)
    }

    return (
        <div className={styles.seatsPicker}>
                {props.seatsScheme.split('!').map((row, rowIdx) => (
                        <div key={uuid()} className={styles.row}>
                            {row.split('').map((seat, seatIdx) => {
                                if (seat == 'B') {
                                    return (
                                        <div key={rowIdx.toString() + seatIdx.toString()} className={styles.occupiedSeat}>
                                            {''}
                                        </div>
                                    )
                                } else if (seat == 'S') {
                                    return <div key={rowIdx.toString() + seatIdx.toString()} className={styles.space}></div>
                                } else if (seat == 'O') {
                                    return (
                                        <div key={rowIdx.toString() + seatIdx.toString()} className={styles.occupiedSeat}>
                                            {''}
                                        </div>
                                    )
                                } else if (seat == 'V') {
                                    return (
                                        <div key={rowIdx.toString() + seatIdx.toString()}
                                             className={seatNumber(rowIdx, seatIdx) == selectedSeat ? styles.selectedSeat : styles.vacantSeat}
                                             onClick={() => {
                                                 setSelectedSeat(seatNumber(rowIdx, seatIdx))
                                                 props.onSeatSelected(getSeatsScheme(rowIdx, seatIdx))
                                             }}
                                        >
                                        </div>
                                    )
                                }

                            })}
                        </div>
                    )
                )}
        </div>
    )
        ;
};

export default SeatPicker;