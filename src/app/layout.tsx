import type { Metadata } from "next";
import { Roboto } from "next/font/google";
import "./globals.scss";
import { CookiesProvider } from 'next-client-cookies/server';
import {redirect} from "next/navigation";

const roboto = Roboto({ weight: ["300", "400", "500", "700", "900"], subsets: ["latin", "cyrillic"] });

export const metadata: Metadata = {
    title: "Межгород | Менеджер",
    description: "Управление поездками",
};

export default function RootLayout({children}: Readonly<{children: React.ReactNode;}>) {

    return (
        <html lang="en">
        <body className={roboto.className}>
        <CookiesProvider>
            {children}
        </CookiesProvider>
        </body>
        </html>
    );
}
