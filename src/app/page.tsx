'use client';

import styles from "./page.module.scss"
import {TripService} from "@/services/trip.service/trip.service";
import Link from "next/link";
import TripItem from "@/components/ui/trip-item/TripItem";
import Image from "next/image"
import {useEffect, useState} from "react";
import {Trip} from "@/entities/trip";
import {StaticDatePicker, StaticDateTimePicker} from "@mui/x-date-pickers";
import dayjs from "dayjs";
import {Modal} from "@mui/material";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import {ruRU} from "@mui/x-date-pickers/locales";
import { LocalizationProvider } from '@mui/x-date-pickers';

const Home = () => {
    const [from, setFrom] = useState('');
    const [to, setTo] = useState('');
    const [trips, setTrips] = useState<Trip[]>([])
    const [isDateTimePickerOpened, setIsDateTimePickerOpened] = useState(false)
    const [searchDate, setSearchDate] = useState<dayjs.Dayjs|null>(null)

    const dateTimeFormat = 'dd DD MMMM'

    useEffect(() => {
        TripService.getAll().then((value) => setTrips(value))

    }, [])

    return (
        <div>
            <LocalizationProvider
                dateAdapter={AdapterDayjs}
                localeText={ruRU.components.MuiLocalizationProvider.defaultProps.localeText}
            >
            <div className={styles.container}>
                <div className={styles.fromToForm}>
                    <div className={`${styles.inputContainer}`}>
                        <Image
                            key="icons/mappin-circle.svg"
                            alt='photo'
                            src="icons/mappin-circle.svg"
                            width={15}
                            height={16}
                            className={styles.mappinIcon}
                        />
                        <input placeholder="Откуда" value={from} onChange={(e) => setFrom(e.target.value)}/>
                    </div>
                    <div className={`${styles.inputContainer} ${styles.destinationInputContainer}`}>
                        <Image
                            key="icons/mappin-circle.svg"
                            alt='photo'
                            src="icons/mappin-circle.svg"
                            width={15}
                            height={16}
                            className={styles.mappinIcon}
                        />
                        <input placeholder="Куда" value={to} onChange={(e) => setTo(e.target.value)}/>
                    </div>
                </div>
                <div className={styles.selectors}>
                    <div
                        className={styles.selectorBtn}
                        onClick={() => setIsDateTimePickerOpened(true)}
                    >
                        <Image
                            key="icons/calendar.svg"
                            alt='photo'
                            src="icons/calendar.svg"
                            width={15}
                            height={14}
                            className={styles.icon}
                        />
                        <div className={styles.text}>{searchDate?.format(dateTimeFormat) ?? 'Все даты'}</div>
                    </div>
                    <div className={styles.selectorBtn}>
                        <Image
                            key="icons/human.svg"
                            alt='photo'
                            src="icons/human.svg"
                            width={15}
                            height={16}
                            className={styles.icon}
                        />
                        <div className={styles.text}>1 человек</div>
                    </div>
                </div>
            </div>
            <div className={styles.tripList}>
                {trips.map((trip) => {
                    if (trip.route.startPlace.name.startsWith(from) && trip.route.endPlace.name.startsWith(to)) {
                        if (searchDate != null && searchDate.isSame(trip.startTime, 'day') || searchDate == null) {
                            return <Link href={'/' + trip.tripID} key={trip.tripID}>
                                <TripItem
                                    key={trip.tripID}
                                    trip={trip}
                                />
                            </Link>
                        }
                    }
                })}
            </div>
            <Modal open={isDateTimePickerOpened} className={'flex flex-column justify-center items-center'} >
                <>
                    <StaticDatePicker
                        className={'!bg-background'}
                        defaultValue={dayjs()}
                        onAccept={(val) => {
                            setSearchDate(val)
                            setIsDateTimePickerOpened(false)
                        }}
                        onClose={() => {
                            setIsDateTimePickerOpened(false)
                        }}
                        disablePast={true}
                    />
                </>
            </Modal>
            </LocalizationProvider>
        </div>
    );
};

export default Home;
