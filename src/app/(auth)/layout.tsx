import "@/app/globals.scss";
import { getCookies } from 'next-client-cookies/server';
import {redirect} from "next/navigation";
import {ManagerAuthService} from "@/services/manager-auth.service/manager-auth.service";

export default function AuthLayout({children}: Readonly<{children: React.ReactNode;}>) {
    const cookies = getCookies()

    const access_token = cookies.get('access_token')

    if (access_token) {
        redirect('/trips')
    }

    return (
        <div>
            {children}
        </div>
    );
}
