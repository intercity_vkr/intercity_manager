'use client'

import Input from "@/components/ui/input/Input";
import Navbar from "@/components/ui/navbar/Navbar";
import Button from "@/components/ui/button/Button";
import {useRouter} from "next/navigation";
import {useState} from "react";
import {ManagerAuthService} from "@/services/manager-auth.service/manager-auth.service";
import {ECodeResult} from "@/services/manager-auth.service/manager-auth.results";

const ConfirmLoginPage = () => {
    const router = useRouter()

    const [code, setCode] = useState('')
    const [wrongCode, setWrongCode] = useState(false)

    return (
        <div>
            <Navbar title={'Подтверждение входа'} showBackButton={true}/>
            <div className={'gap-5 flex flex-col mt-10'}>
                <Input
                    placeholder={'Код из SMS'}
                    value={code}
                    onChange={(val) => setCode(val.target.value)}
                />
                <Button text={'Войти'} onClick={async () => {
                    const res = await ManagerAuthService.code(code)
                    if (res == ECodeResult.Success) {
                        router.replace('/trips')

                    } else {
                        setWrongCode(true)
                    }
                }}/>
                {wrongCode ? <p className={'text-center text-red'}>Неверный код</p> : <div></div>}
            </div>
        </div>
    );
};

export default ConfirmLoginPage;