import Input from "@/components/ui/input/Input";
import Navbar from "@/components/ui/navbar/Navbar";
import Button from "@/components/ui/button/Button";
import {useRouter} from "next/navigation";
// import {useState} from "react";
import {ManagerAuthService} from "@/services/manager-auth.service/manager-auth.service";
import {EInitAuthResult} from "@/services/manager-auth.service/manager-auth.results";

const LoginPage = () => {
    // const router = useRouter()
    // const [phone, setPhone] = useState('')
    // const [userNotFound, setUserNotFound] = useState(false)
    // const [unknownError, setUnknownError] = useState(false)

    return (
        <div>
            <Navbar title={'Вход'}/>
            <div className={'gap-5 flex flex-col mt-10'}>
                <Input
                    placeholder={'Номер телефона'}
                    // value={phone}
                    // onChange={(value) => {setPhone(value.target.value)}}
                />
                <Button text={'Войти'} />
                {/*{userNotFound ? <p className={'text-red text-center'}>Пользователь не найден</p> : <div></div>}*/}
                {/*{unknownError ? <p className={'text-red text-center'}>Пользователь не найден</p> : <div></div>}*/}
            </div>
        </div>
    );
};

export default LoginPage;