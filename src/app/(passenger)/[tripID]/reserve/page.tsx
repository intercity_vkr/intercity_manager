'use client';

import React, {useEffect, useState} from 'react';
import Navbar from "@/components/ui/navbar/Navbar";
import Heading from "@/components/ui/heading/Heading";
import styles from "@/app/(app)/vehicles/[vehicleID]/edit/page.module.scss";
import Input from "@/components/ui/input/Input";
import Button from "@/components/ui/button/Button";
import {VehicleService} from "@/services/vehicle.service";
import {useRouter} from "next/navigation";
import {emptyTrip} from "@/entities/trip";
import {TripService} from "@/services/trip.service/trip.service";
import { v4 as uuidv4 } from 'uuid';
import SeatPicker from "@/components/ui/seat-picker/SeatPicker";
import {Passenger} from "@/entities/passenger";

const ReserveTripPage = ({params}: {params: {tripID: string}}) => {
    const [passenger, setPassenger] = useState<Passenger>({firstName: "", lastName: "", passengerID: "", phone: ""})
    const [trip, setTrip] = useState(emptyTrip)
    const [seatsScheme, setSeatsScheme] = useState('')
    const [success, setSuccess] = useState(false)

    useEffect(() => {
        TripService.getByID(params.tripID).then(value => {
            if (value) {
                setTrip(value)
                setSeatsScheme(value.car.seatingChart)
            }

        })
    }, [])

    const router = useRouter()

    return (
        <div>
            <Navbar title={''} showBackButton={true}/>
            <Heading className={styles.heading} text={'Бронирование поездки'}/>
            <div className={styles.inputs}>
                <Input
                    placeholder={'Имя'}
                    value={passenger.firstName}
                    onChange={(val) => setPassenger({...passenger, firstName: val.target.value})}
                />
                <Input
                    placeholder={'Номер телефона'}
                    value={passenger.phone}
                    onChange={(val) => setPassenger({...passenger, phone: val.target.value})}
                />
            </div>
            <div className={styles.seatsPicker}>
                <SeatPicker
                    seatsScheme={seatsScheme}
                    onSeatSelected={newSeatScheme => {}}
                />
            </div>
            <Button className={styles.button} text={'Забронировать'} onClick={() => {
                // if (passenger.name && passenger.phone) {
                //     const passengerData = {...passenger, id: uuidv4()}
                //     TripService.putById(params.tripID, {...trip, passengers: [...trip.passengers, passengerData]})
                //     setSuccess(true)
                // }
            }}/>
            {success && <p className={'text-center text-green2 mt-4'}>Поездка успешно забронирована</p>}
        </div>
    );
};

export default ReserveTripPage;