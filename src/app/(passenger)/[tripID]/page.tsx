'use client'

import Heading from "@/components/ui/heading/Heading";
import styles from "./page.module.scss"
import Image from "next/image";
import RegularTextWithIcon from "@/components/ui/regulat-text-with-icon/RegularTextWithIcon";
import {TripService} from "@/services/trip.service/trip.service";
import DeleteButton from "./_components/DeleteButton";
import EditButton from "./_components/ReserveButton";
import Link from "next/link";
import Navbar from "@/components/ui/navbar/Navbar";
import dayjs from "dayjs";
import ReserveButton from "./_components/ReserveButton";
import {StaticDatePicker} from "@mui/x-date-pickers";
import {Modal} from "@mui/material";
import {emptyTrip, Trip} from "@/entities/trip";
import {useEffect, useState} from "react";
import Button from "@/components/ui/button/Button";
import {GigachatService} from "@/services/gigachat.service/gigachat.service";

const TripDetailPage = ({params}: {params: {tripID: string}}) => {
    const [trip, setTrip] = useState<Trip>(emptyTrip)
    // const [startDate, setStartDate] = useState(dayjs())
    // const [endDate, setEndDate] = useState(dayjs())
    const [isGigachatOpen, setIsGigachatOpen] = useState(false)
    const [gigachatText, setGigachatText] = useState('')

    useEffect(() => {
        TripService.getByID(params.tripID).then(val => {
            if (val) {
                setTrip(val)
            }
        })
        // setStartDate(dayjs(trip.startDateTime))
        // setEndDate(dayjs(trip.startDateTime).add(trip.durationMinutes, 'minute'))
    }, [])

    return (
        <div>
            <Navbar title={''} showBackButton={true}/>
            <Heading className={styles.heading} text={dayjs(trip.startTime).format('DD MMMM dddd')}/>
            <div className={styles.route}>
                <Heading text={trip.route.startPlace.name}/>
                <Image className={styles.arrow} src={'/decoration/arrow-right.svg'} alt={''} width={27} height={15}/>
                <Heading text={trip.route.endPlace.name}/>
                <div className={styles.time1}>{dayjs(trip.startTime).format('HH:mm')}</div>
                <div className={styles.time2}>{dayjs(trip.endTime).format('HH:mm')}</div>
            </div>
            <p className={'my-2 text-green2'} onClick={() => {
                GigachatService.getCityDescription(trip.route.startPlace.name).then(res => {
                    if (res) {
                        setGigachatText(res)
                        setIsGigachatOpen(true)
                    }
                })
            }}>Узнать о городах маршрута у Gigachat</p>
            {
                trip.route.stops ?
                    <>
                        <Heading className={styles.stopListHeading} text={'Остановки по пути'}/>
                        <div className={styles.stopList}>
                            {
                                trip.route.stops.map((stop) => (
                                    <div className={styles.stop} key={stop.placeID}>
                                        <p>{stop.name}</p>
                                        {/*<p>{stop.}</p>*/}
                                    </div>
                                ))
                            }
                        </div>
                    </>
                :
                <Heading className={styles.stopListHeading} text={'Без остановок по пути'}/>
            }
            <hr/>
            <div className={styles.infoList}>
                <RegularTextWithIcon text={dayjs(trip.startTime).diff(dayjs(trip.endTime), 'h') + ' ч в пути'} iconSrc={'/icons/clock.svg'}/>
                <RegularTextWithIcon text={trip.car.mark + ' ' + trip.car.model} iconSrc={'/icons/car.svg'}/>
                <RegularTextWithIcon text={'Перевозчик ' + trip.company.name} iconSrc={'/icons/sheet.svg'}/>
            </div>
            <hr/>
            <div className={styles.seatsInfo}>
                <p>{3}/{6} места свободно</p>
                <div className={styles.price}>
                    <p className={styles.text}>Одно место</p>
                    <p>{1000}</p>
                </div>
            </div>
            <hr/>
            {/*<Heading className={styles.passengersHeading} text={'Пассажиры'}/>*/}
            {/*<div className={styles.passengerList}>*/}
            {/*    {*/}
            {/*        trip.confirmedPassengers.map((passenger, idx, passengers) => (*/}
            {/*            <div key={passenger.passenger.passengerID}>*/}
            {/*                <Link href={`/trips/${trip.tripID}/passengers/${passenger.passenger.passengerID}`}>*/}
            {/*                    <div className={styles.passenger}>*/}
            {/*                        <p className={styles.textGreen}>{passenger.passenger.firstName}</p>*/}
            {/*                        <Image src={'/icons/chevron-right-sm.svg'} alt={''} width={5} height={10}/>*/}
            {/*                    </div>*/}
            {/*                </Link>*/}
            {/*                {idx + 1 < passengers.length ? <hr/> : <div></div>}*/}
            {/*            </div>*/}
            {/*        ))*/}
            {/*    }*/}
            {/*    {*/}
            {/*        trip.requestPassengers.map((passenger, idx, passengers) => (*/}
            {/*            <div key={passenger.passenger.passengerID}>*/}
            {/*                <Link href={`/trips/${trip.tripID}/passengers/${passenger.passenger.passengerID}`}>*/}
            {/*                    <div className={styles.passenger}>*/}
            {/*                        <p className={styles.textGrey}>{passenger.passenger.firstName}</p>*/}
            {/*                        <Image src={'/icons/chevron-right-sm.svg'} alt={''} width={5} height={10}/>*/}
            {/*                    </div>*/}
            {/*                </Link>*/}
            {/*                {idx + 1 < passengers.length ? <hr/> : <div></div>}*/}
            {/*            </div>*/}
            {/*        ))*/}
            {/*    }*/}
            {/*</div>*/}
            <div className={styles.buttons}>
                <ReserveButton id={trip.tripID}/>
            </div>
            <Modal open={isGigachatOpen} className={'flex flex-column justify-center items-center'}>
                <div className={'bg-white p-16'}>
                    {gigachatText}
                    <p className={'text-red text-center'}>Закрыть</p>
                </div>
            </Modal>
        </div>
    );
};

export default TripDetailPage;