'use client';

import Button from "@/components/ui/button/Button";
import {TripService} from "@/services/trip.service";
import {useRouter} from "next/navigation";

interface IReserveButtonProps {
    id: string
}

const ReserveButton = (props: IReserveButtonProps) => {
    const router = useRouter()

    return (
        <Button text={'Забронировать место'} onClick={() => {
            router.push(`/${props.id}/reserve`)
        }}/>
    );
};

export default ReserveButton;