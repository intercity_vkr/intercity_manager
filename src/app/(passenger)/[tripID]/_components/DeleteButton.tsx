'use client';

import Button from "@/components/ui/button/Button";
import {TripService} from "@/services/trip.service";
import {useRouter} from "next/navigation";

interface IDeleteButtonProps {
    id: string
}

const DeleteButton = (props: IDeleteButtonProps) => {
    const router = useRouter()

    return (
        <Button text={'Удалить поездку'} className={'!bg-red'} textClassName={'text-white'} onClick={() => {
            TripService.deleteById(props.id)
            router.replace('/trips')
            router.refresh()
        }}/>
    );
};

export default DeleteButton;