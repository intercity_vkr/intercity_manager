"use client"

import Heading from "@/components/ui/heading/Heading";
import styles from "./page.module.scss";
import Input from "@/components/ui/input/Input";
import Button from "@/components/ui/button/Button";
import Navbar from "@/components/ui/navbar/Navbar";
import {useEffect, useState} from "react";
import {CarService} from "@/services/car.service/car.service";
import {useRouter} from "next/navigation";
import {CarEntity, emptyCar} from "@/entities/car";

const EditVehiclePage = ({params}: {params: {vehicleID: string}}) => {
    const [car, setCar] = useState<CarEntity>(emptyCar)

    useEffect(() => {
        CarService.getById(params.vehicleID).then(value => { if (value != null) {
            setCar(value)
        } })
    }, [])

    const router = useRouter()

    return (
        <div>
            <Navbar title={''} showBackButton={true}/>
            <Heading className={styles.heading} text={'Изменение транспортного средства'}/>
            <div className={styles.inputs}>
                <Input
                    placeholder={'Марка'}
                    value={car.mark}
                    onChange={(val) => setCar({...car, mark: val.target.value})}
                />
                <Input
                    placeholder={'Модель'}
                    value={car.model}
                    onChange={(val) => setCar({...car, model: val.target.value})}
                />
                <Input
                    placeholder={'Гос. номер'}
                    value={car.gosnomer}
                    onChange={(val) => setCar({...car, gosnomer: val.target.value})}
                />
                <Input
                    placeholder={'Цвет'}
                    value={car.color}
                    onChange={(val) => setCar({...car, color: val.target.value})}
                />
                <Input
                    placeholder={'Схема рассадки'}
                    value={car.seatingChart}
                    onChange={(val) => setCar({...car, seatingChart: val.target.value})}
                />
            </div>
            <Button className={styles.button} text={'Изменить'} onClick={() => {
                CarService.update(car.carID, {color: car.color, gosnomer: car.gosnomer, mark: car.mark, model: car.model, seatingChart: car.seatingChart})
                router.replace('/vehicles')
                router.refresh()
            }}/>
        </div>
    );
};
export default EditVehiclePage;