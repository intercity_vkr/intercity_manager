'use client'

import Heading from "@/components/ui/heading/Heading";
import Caption from "@/components/ui/caption/Caption";
import styles from "./page.module.scss"
import {CarService} from "@/services/car.service/car.service";
import Navbar from "@/components/ui/navbar/Navbar";
import EditButton from "./_components/EditButton";
import DeleteButton from "./_components/DeleteButton";
import { CarEntity } from "@/entities/car";
import {useEffect, useState} from "react";

const VehicleDetailPage = ({params}: {params: {vehicleID: string}}) => {
    const [car, setCar] = useState<CarEntity>()

    useEffect(() => {
        CarService.getById(params.vehicleID).then((car) => {
            if (car != null) {
                setCar(car)
            }
        })
    }, [])

    return (
        <div>
            <Navbar title={''} showBackButton={true}/>
            <Heading className={styles.heading} text={'Транспортное средство'}/>
            <div className={styles.infoList}>
                <div className={styles.item}>
                    <Caption captionText={'Марка'}/>
                    <p>{car?.mark}</p>
                </div>
                <div className={styles.item}>
                    <Caption captionText={'Модель'}/>
                    <p>{car?.model}</p>
                </div>
                <div className={styles.item}>
                    <Caption captionText={'Гос. номер'}/>
                    <p>{car?.gosnomer}</p>
                </div>
                <div className={styles.item}>
                    <Caption captionText={'Цвет'}/>
                    <p>{car?.color}</p>
                </div>
                <div className={styles.item}>
                    <Caption captionText={'Схема рассадки'}/>
                    <p>{car?.seatingChart}</p>
                </div>
            </div>
            <div className={styles.buttonList}>
                <EditButton id={car?.carID ?? ''}/>
                <DeleteButton id={car?.carID ?? ''}/>
            </div>
        </div>
    );
};

export default VehicleDetailPage;