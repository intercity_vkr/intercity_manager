'use client';

import Button from "@/components/ui/button/Button";
import {TripService} from "@/services/trip.service";
import {useRouter} from "next/navigation";

interface IEditButtonProps {
    id: string
}

const EditButton = (props: IEditButtonProps) => {
    const router = useRouter()

    return (
        <Button text={'Изменить'} onClick={() => {
            router.push(`/vehicles/${props.id}/edit`)
        }}/>
    );
};

export default EditButton;