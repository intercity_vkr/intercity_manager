'use client';

import Button from "@/components/ui/button/Button";
import {useRouter} from "next/navigation";
import {VehicleService} from "@/services/vehicle.service";

interface IDeleteButtonProps {
    id: string
}

const DeleteButton = (props: IDeleteButtonProps) => {
    const router = useRouter()

    return (
        <Button text={'Удалить'} className={'!bg-red'} textClassName={'text-white'} onClick={() => {
            // CarService.deleteById(props.id)
            router.replace('/vehicles')
            router.refresh()
        }}/>
    );
};

export default DeleteButton;