'use client';

import Heading from "@/components/ui/heading/Heading";
import styles from "./page.module.scss";
import ListItem from "@/components/ui/list-item/ListItem";
import Icon from "@/components/ui/icon/Icon";
import IconButton from "@/components/ui/icon-button/IconButton";
import {CarEntity} from "@/entities/car";
import {CarService} from "@/services/car.service/car.service";
import Link from "next/link";
import Navbar from "@/components/ui/navbar/Navbar";
import {useEffect, useState} from "react";

const VehiclesPage = () => {
    const [vehicles, setVehicles] = useState<CarEntity[]>([])

    useEffect(() => {
        CarService.getAll().then((cars) => {
            setVehicles(cars.sort((a, b) => { if (a.mark > b.mark) {return 1} else {return -1}}))
        })
    }, [])

    return (
        <div>
            <Navbar title={'ИП Танкоград'} showBackButton={false}/>
            <div className={styles.heading}>
                <Heading text={'Транспортные средства'}/>
                <Link href={'/vehicles/create'}>
                    <IconButton iconSrc={'icons/plus-square.svg'}/>
                </Link>
            </div>
            {vehicles.map((car, idx) => (
                <div key={car.carID}>
                    <Link href={'/vehicles/' + car.carID}>
                        <ListItem
                            text={car.mark + ' ' + car.model}
                            right={<Icon src={'/icons/chevron-right-sm.svg'} width={5} height={10}/>}
                        />
                    </Link>
                    {idx + 1 != vehicles.length ? <hr/> : <div></div>}
                </div>
            ))
            }
        </div>
    );
};

export default VehiclesPage;