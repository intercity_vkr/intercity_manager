import Tabbar from "@/components/ui/tabbar/Tabbar";
import { getCookies } from 'next-client-cookies/server';
import {redirect} from "next/navigation";

export default function AppLayout({children}: Readonly<{children: React.ReactNode;}>) {
    const cookies = getCookies()

    const access_token = cookies.get('access_token')

    if (!access_token) {
        redirect('/login')
    }

    return (
        <div>
            {children}
            <Tabbar/>
        </div>
    );
}
