'use client'

import IconButton from "@/components/ui/icon-button/IconButton";
import TripItem from "@/components/ui/trip-item/TripItem";
import Heading from "@/components/ui/heading/Heading";
import styles from "./page.module.scss"
import {TripService} from "@/services/trip.service/trip.service";
import Link from "next/link";
import Navbar from "@/components/ui/navbar/Navbar";
import {useEffect, useState} from "react";
import {Trip} from "@/entities/trip";

const TripsPage = () => {
    const [trips, setTrips] = useState<Trip[]>([])

    useEffect(() => {
        TripService.getAll().then(trips => {
            console.log(trips)
            setTrips(trips)
        })
    }, []);

    return (
        <div>
            <Navbar title={'ИП Танкоград'}/>
            <div className={styles.heading}>
                <Heading text={'Ближайшие поездки'}/>
                <Link href={'/trips/create'}>
                    <IconButton iconSrc={'icons/plus-square.svg'}/>
                </Link>
            </div>
            <div className={styles.tripList}>
                {trips.map((trip) => (
                    <Link href={'/trips/' + trip.tripID} key={trip.tripID}>
                        <TripItem
                            key={trip.tripID}
                            trip={trip}
                        />
                    </Link>
                ))}
            </div>
        </div>
    );
};

export default TripsPage;
