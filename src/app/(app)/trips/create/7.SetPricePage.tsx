'use client'

import Heading from "@/components/ui/heading/Heading";
import styles from "./page.module.scss";
import Input from "@/components/ui/input/Input";
import Button from "@/components/ui/button/Button";
import ICreateTripFormPageProps from "./ICreateTripFormPageProps.interface";

const SetPricePage = (props: ICreateTripFormPageProps) => {
    return (
        <div>
            <Heading className={styles.heading} text={'Сколько стоит место?'}/>
            <Input
                placeholder={'Укажите цену'}
                type={'number'}
                value={props.tripData.price}
                onChange={e => { props.setTripData({...props.tripData, price: +e.target.value}) }}
            />
        </div>
    );
};

export default SetPricePage