import {Dispatch, SetStateAction} from "react";
import {CreateTrip} from "./tripFormData";
import {CreateTripInput} from "@/entities/trip";
import {Route} from "@/entities/route";

export default interface ICreateTripFormPageProps {
    tripData: CreateTripInput
    setTripData: Dispatch<SetStateAction<CreateTripInput>>
    routeData: Route
    setRouteData: Dispatch<SetStateAction<Route>>
}