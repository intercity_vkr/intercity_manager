import {emptyTrip, Trip} from "@/entities/trip";

export type CreateTrip = {
    trip: Trip
    extraStartDates: string[]
}

export const emptyTripFormData = (): CreateTrip => {
    return {trip: emptyTrip, extraStartDates: []}
};