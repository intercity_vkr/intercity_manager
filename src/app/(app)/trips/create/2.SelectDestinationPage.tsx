'use client'

import Heading from "@/components/ui/heading/Heading";
import styles from "./page.module.scss";
import Input from "@/components/ui/input/Input";
import ListItem from "@/components/ui/list-item/ListItem";
import Icon from "@/components/ui/icon/Icon";
import ICreateTripFormPageProps from "./ICreateTripFormPageProps.interface";
import {useEffect, useState} from "react";
import {Place} from "@/entities/place";
import {PlaceService} from "@/services/place.service/place.service";

const SelectDestinationPage = (props: ICreateTripFormPageProps) => {
    const [places, setPlaces] = useState<Place[]>([])

    useEffect(() => {
        PlaceService.getAll().then((value) => {
            setPlaces(value)
        })
    }, []);

    return (
        <div>
            <Heading className={styles.heading} text={'Куда едем?'}/>
            <Input
                className={styles.input}
                placeholder={'Укажите адрес'}
                value={props.routeData.endPlace.name}
                // onChange={e => { props.setTripFormData({...props.tripFormData, from: e.target.value}) }}
                onChange={e => {
                    // props.setTripFormData({...props.tripData, trip: {...props.tripData.trip, from: e.target.value}})
                }}
            />
            <ListItem
                text={'Указать на карте'}
                left={<Icon src={'/icons/pin.svg'}/>}
                right={<Icon src={'/icons/chevron-right-sm.svg'} width={5} height={10}/>}
            />
            {places.map((place) => (
                <div key={place.placeID} onClick={() => {
                    props.setRouteData({...props.routeData, endPlace: place})
                }}>
                    <hr/>
                    <ListItem
                        text={place.name}
                        left={<Icon src={'/icons/clock.svg'}/>}
                        right={<Icon src={'/icons/chevron-right-sm.svg'} width={5} height={10}/>}
                    />
                </div>
            ))}
        </div>
    );
};

export default SelectDestinationPage