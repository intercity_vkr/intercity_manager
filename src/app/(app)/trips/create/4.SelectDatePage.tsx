'use client'

import Heading from "@/components/ui/heading/Heading";
import styles from "./page.module.scss";
import {StaticDateTimePicker} from "@mui/x-date-pickers";
import dayjs from "dayjs";
import ICreateTripFormPageProps from "./ICreateTripFormPageProps.interface";

const SelectDatePage = (props: ICreateTripFormPageProps) => {
    return (
        <div>
            <Heading className={styles.heading} text={'Выберите дату и время поездки'}/>
            <div className={'flex flex-col items-center'}>
                <StaticDateTimePicker
                    className={'!bg-transparent'}
                    value={dayjs(props.tripData.startTime)}
                    onChange={val => {
                        props.setTripData({...props.tripData, startTime: val?.toDate().toISOString() ?? ''})
                    }}
                />
            </div>
        </div>

    );
};

export default SelectDatePage