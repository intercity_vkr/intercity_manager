'use client';

import Heading from "@/components/ui/heading/Heading";
import styles from "./page.module.scss";
import ListItem from "@/components/ui/list-item/ListItem";
import Checkbox from "@/components/ui/checkbox/Checkbox";
import ICreateTripFormPageProps from "./ICreateTripFormPageProps.interface";
import {Place} from "@/entities/place";
import {useEffect, useState} from "react";
import {PlaceService} from "@/services/place.service/place.service";

const SelectStopsPage = (props: ICreateTripFormPageProps) => {
    const [stops, setStops] = useState<Place[]>([])

    useEffect(() => {
        PlaceService.getAll().then(res => setStops(res))
    }, []);

    const addStop = (stop: Place) => {
        props.setRouteData({...props.routeData, stops: [...props.routeData.stops, stop]})
    }

    const deleteStop = (stop: Place) => {
        const stops = props.routeData.stops.filter((val) => JSON.stringify(val) != JSON.stringify(stop))
        props.setRouteData({...props.routeData, stops: stops})
    }

    const handleCheckboxClick = (stop: Place) => {
        if (props.routeData.stops?.find(e => JSON.stringify(e) == JSON.stringify(stop))) {
            deleteStop(stop)
        } else {
            addStop(stop)
        }
    }

    return (
        <div>
            <Heading className={styles.heading} text={'Выберите остановки по пути'}/>
            <ListItem
                text={'Указать на карте'}
                textClassName={styles.greenText}
            />
            <hr/>
            {stops.map((stop, idx) => (
                <div key={stop.placeID}>
                    <ListItem
                        text={stop.name}
                        right={<Checkbox
                            key={stop.placeID}
                            checked={props.routeData.stops?.find(e => JSON.stringify(e) == JSON.stringify(stop)) != undefined}
                            onChange={() => {
                                handleCheckboxClick(stop)
                            }}
                        />}
                    />
                    {idx + 1 < stops.length ? <hr/> : <div></div>}
                </div>
            ))}
        </div>
    );
};

export default SelectStopsPage