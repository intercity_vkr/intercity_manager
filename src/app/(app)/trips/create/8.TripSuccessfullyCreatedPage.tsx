import Image from "next/image";

const TripSuccessfullyCreatedPage = () => {
    return (
        <div className={'flex flex-col items-center justify-center mt-52'}>
            <Image src={'/images/success.svg'} alt={''} width={125} height={125}/>
            <p className={'text-xl font-bold mt-4 mb-52'}>Поездка создана</p>
        </div>
    );
};

export default TripSuccessfullyCreatedPage