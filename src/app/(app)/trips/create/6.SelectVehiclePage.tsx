import Heading from "@/components/ui/heading/Heading";
import styles from "./page.module.scss";
import ListItem from "@/components/ui/list-item/ListItem";
import {CarService} from "@/services/car.service/car.service";
import {useEffect, useState} from "react";
import ICreateTripFormPageProps from "./ICreateTripFormPageProps.interface";
import Checkbox from "@/components/ui/checkbox/Checkbox";
import {CarEntity} from "@/entities/car";

const SelectVehiclePage = (props: ICreateTripFormPageProps) => {
    const [vehicles, setVehicles] = useState<CarEntity[]>([])

    useEffect(() => {
        CarService.getAll().then((res) => setVehicles(res))
    }, [])

    const handleCheckboxClick = (vehicle: CarEntity) => {
        if (props.tripData.carID == '' || props.tripData.carID != vehicle.carID) {
            props.setTripData({...props.tripData, carID: vehicle.carID})
        } else {
            props.setTripData({...props.tripData, carID: ''})
        }
    }

    return (
        <div>
            <Heading className={styles.heading} text={'Выберите транспортное средство'}/>
            {vehicles.map((vehicle, idx) => (
                <div key={vehicle.carID}>
                    <ListItem
                        text={vehicle.mark + ' ' + vehicle.model + ' ' + vehicle.color}
                        right={
                            <Checkbox
                                checked={props.tripData.carID == vehicle.carID}
                                onChange={() => {
                                    handleCheckboxClick(vehicle)
                                }}
                            />
                        }
                    />
                    {idx + 1 != vehicles.length ? <hr/> : <div></div> }
                </div>
            ))}
        </div>
    );
};

export default SelectVehiclePage