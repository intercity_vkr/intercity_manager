'use client'

import SelectDeparturePage from "./1.SelectDeparturePage";
import { useState } from 'react';
import Button from "@/components/ui/button/Button";
import styles from "./page.module.scss"
import SelectDestinationPage from "./2.SelectDestinationPage";
import SelectStopsPage from "./3.SelectStopsPage";
import SelectDatePage from "./4.SelectDatePage";
import AddDatesPage from "./5.SelectEndDatePage";
import SelectVehiclePage from "./6.SelectVehiclePage";
import SetPricePage from "./7.SetPricePage";
import TripSuccessfullyCreatedPage from "./8.TripSuccessfullyCreatedPage";
import {useRouter} from "next/navigation";
import Navbar from "@/components/ui/navbar/Navbar";
import {emptyTripFormData, CreateTrip} from "./tripFormData";
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import 'dayjs/locale/de';
import { ruRU } from '@mui/x-date-pickers/locales';
import {TripService} from "@/services/trip.service/trip.service";
import {CreateTripInput, emptyTrip} from "@/entities/trip";
import {Route} from "@/entities/route";
import dayjs from "dayjs";
import SelectEndDatePage from "./5.SelectEndDatePage";
import {RouteService} from "@/services/route.service/route.service";

const CreateTripPage = () => {
    const router = useRouter()
    const [page, setPage] = useState(0)
    const [tripData, setTripData] = useState<CreateTripInput>({
        price: 0,
        carID: "",
        endTime: dayjs().toDate().toISOString(),
        reversed: false,
        routeID: "",
        startTime: dayjs().toDate().toISOString(),
        tripSeatingChart: ""
    })

    const [routeData, setRouteData] = useState<Route>({
        description: "",
        endPlace: {
            placeID: "",
            name: "",
            point: {
                latitude: 0,
                longitude: 0
            }
        },
        name: "",
        routeID: "",
        startPlace: {
            placeID: "",
            name: "",
            point: {
                latitude: 0,
                longitude: 0
            }
        },
        stops: []
    })

    const PageDisplay = () => {
        switch (page) {
            case 0: { return <SelectDeparturePage tripData={tripData} setTripData={setTripData} routeData={routeData} setRouteData={setRouteData}/> }
            case 1: { return <SelectDestinationPage tripData={tripData} setTripData={setTripData} routeData={routeData} setRouteData={setRouteData}/> }
            case 2: { return <SelectStopsPage tripData={tripData} setTripData={setTripData} routeData={routeData} setRouteData={setRouteData}/> }
            case 3: { return <SelectDatePage tripData={tripData} setTripData={setTripData} routeData={routeData} setRouteData={setRouteData}/> }
            case 4: { return <SelectEndDatePage tripData={tripData} setTripData={setTripData} routeData={routeData} setRouteData={setRouteData}/> }
            case 5: { return <SelectVehiclePage tripData={tripData} setTripData={setTripData} routeData={routeData} setRouteData={setRouteData}/> }
            case 6: { return <SetPricePage tripData={tripData} setTripData={setTripData} routeData={routeData} setRouteData={setRouteData}/> }
            case 7: { return <TripSuccessfullyCreatedPage/> }
        }
    }

    const isButtonDisabled = () => {
        switch (page) {
            case 0: { return !routeData.startPlace.name;  }
            case 1: { return !routeData.endPlace.name }
            case 2: { return !routeData.stops || routeData.stops.length == 0 }
            case 3: { return !tripData.startTime }
            case 5: { return !tripData.carID }
            case 6: { return false }
        }
        return false
    }

    const getOrCreateRoute = async () => {
        const routes = await RouteService.filter({start_place_id: routeData.startPlace.placeID, end_place_id: routeData.endPlace.placeID})
        if (routes && routes.length > 0) {
            setTripData({...tripData, routeID: routes[0].routeID})
        }
        const createdRoute = await RouteService.create({
            description: "",
            endPlaceID: routeData.endPlace.placeID,
            name: `${routeData.startPlace.name} - ${routeData.endPlace.name}`,
            startPlaceID: routeData.startPlace.placeID,
            stopsIDs: routeData.stops.map(stop => stop.placeID)
        })
        setTripData({...tripData, routeID: createdRoute.routeID})
    }

    const onNextButtonClick = () => {
        console.log(tripData)
        if (page == 2) {
            getOrCreateRoute()
        }
        if (page == 6) {
            createTrip(tripData)
        }
        if (page == 7) {
            router.replace('/trips')
            router.refresh()
            return
        }
        setPage((currPage) => currPage + 1)
    }

    const onBackButtonClick = () => {
        if (page == 0) {
            router.replace('/trips')
        } else {
            setPage((currPage) => currPage - 1)
        }
    }

    const createTrip = (tripFormData: CreateTripInput) => {
        TripService.create(tripFormData)
    }

    return (
        <div>
            <Navbar title={''} showBackButton={true} backButtonAction={onBackButtonClick}/>
            <LocalizationProvider
                dateAdapter={AdapterDayjs}
                // adapterLocale="de"
                localeText={ruRU.components.MuiLocalizationProvider.defaultProps.localeText}
            >
            { PageDisplay() }
            </LocalizationProvider>
            <Button
                className={styles.button}
                disabled={isButtonDisabled()}
                text={page == 7 ? 'Готово' : page == 6 ? 'Создать поездку' : 'Далее'}
                onClick={() => {
                    onNextButtonClick()
                }}
            />
        </div>
    );
};

export default CreateTripPage;
