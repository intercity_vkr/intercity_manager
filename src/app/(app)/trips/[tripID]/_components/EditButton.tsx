'use client';

import Button from "@/components/ui/button/Button";
import {TripService} from "@/services/trip.service";
import {useRouter} from "next/navigation";

interface IEditButtonProps {
    id: string
}

const EditButton = (props: IEditButtonProps) => {
    const router = useRouter()

    return (
        <Button text={'Редактировать поездку'} onClick={() => {
            router.push(`/trips/${props.id}/edit`)
        }}/>
    );
};

export default EditButton;