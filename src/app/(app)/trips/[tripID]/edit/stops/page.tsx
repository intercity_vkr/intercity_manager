'use client';

import Heading from "@/components/ui/heading/Heading";
import styles from "./page.module.scss";
import StopsPicker from "@/components/ui/stops-picker/StopsPicker";
import Navbar from "@/components/ui/navbar/Navbar";
import {emptyTrip} from "@/entities/trip";
import {useEffect, useState} from "react";
import {TripService} from "@/services/trip.service";
import Button from "@/components/ui/button/Button";
import {useRouter} from "next/navigation";

const EditStopsPage = ({params}: {params: {tripID: string}}) => {
    const [trip, setTrip] = useState(emptyTrip())
    const [initialTrip, setInitialTrip] = useState(emptyTrip())

    useEffect(() => {
        TripService.getById(params.tripID).then(res => {
            setTrip(res)
            setInitialTrip(res)
        })
    }, [])

    const router = useRouter()

    return (
        <div>
            <Navbar title={''} showBackButton={true}/>
            <Heading className={styles.heading} text={'Выберите остановки по пути'}/>
            <StopsPicker
                stops={initialTrip.stops}
                selectedStops={trip.stops}
                onChange={(value) => {
                    setTrip({...trip, stops: value})
                }}
            />
            <Button
                className={styles.button}
                text={'Готово'}
                onClick={() => {
                    TripService.putById(params.tripID, trip)
                    router.replace(`/trips/${params.tripID}/`)
                    router.refresh()
                }}
            />
        </div>
    );
};

export default EditStopsPage