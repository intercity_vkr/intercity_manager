import Heading from "@/components/ui/heading/Heading";
import styles from "./page.module.scss";
import ListItem from "@/components/ui/list-item/ListItem";
import Icon from "@/components/ui/icon/Icon";
import Navbar from "@/components/ui/navbar/Navbar";
import Link from "next/link";

const EditTripPage = ({params}: {params: {tripID: string}}) => {
    return (
        <div>
            <Navbar title={''} showBackButton={true}/>
            <Heading className={styles.heading} text={'Изменение данных о поездке'}/>
            <Link href={`/trips/${params.tripID}/edit/departure`}>
                <ListItem
                    text={'Пункт отправления'}
                    right={<Icon src={'/icons/chevron-right-sm.svg'} width={5} height={10}/>}
                />
            </Link>
            <hr/>
            <Link href={`/trips/${params.tripID}/edit/destination`}>
                <ListItem
                    text={'Пункт назначения'}
                    right={<Icon src={'/icons/chevron-right-sm.svg'} width={5} height={10}/>}
                />
            </Link>
            <hr/>
            <Link href={`/trips/${params.tripID}/edit/stops`}>
                <ListItem
                    text={'Остановки'}
                    right={<Icon src={'/icons/chevron-right-sm.svg'} width={5} height={10}/>}
                />
            </Link>
            <hr/>
            <Link href={`/trips/${params.tripID}/edit/vehicle`}>
                <ListItem
                    text={'Транспортное средство'}
                    right={<Icon src={'/icons/chevron-right-sm.svg'} width={5} height={10}/>}
                />
            </Link>
            <hr/>
            <ListItem
                text={'Цена'}
                right={<Icon src={'/icons/chevron-right-sm.svg'} width={5} height={10}/>}
            />
            <hr/>
            <ListItem
                text={'Даты'}
                right={<Icon src={'/icons/chevron-right-sm.svg'} width={5} height={10}/>}
            />
        </div>
    );
};

export default EditTripPage;