"use client"

import React, {useEffect, useState} from 'react';
import VehiclePicker from "@/components/ui/vehicle-picker/VehiclePicker";
import {emptyTrip} from "@/entities/trip";
import {TripService} from "@/services/trip.service/trip.service";
import Navbar from "@/components/ui/navbar/Navbar";
import Button from "@/components/ui/button/Button";
import {useRouter} from "next/navigation";

const EditVehiclePage = ({params}: {params: {tripID: string}}) => {
    const [trip, setTrip] = useState(emptyTrip)

    useEffect(() => {
        TripService.getByID(params.tripID).then(res => {
            if (res) {
                setTrip(res)
            }
        })
    }, [])

    const router = useRouter()

    return (
        <div>
            <Navbar title={''} showBackButton={true}/>
            <VehiclePicker
                selectedVehicle={trip.car}
                onChange={vehicle => setTrip({...trip, car: vehicle})}
            />
            <Button text={'Готово'} onClick={() => {
                TripService.update(params.tripID, {
                    carID: trip.car.carID,
                    endTime: trip.endTime,
                    price: trip.price,
                    reversed: trip.reversed,
                    routeID: trip.route.routeID,
                    startTime: trip.startTime,
                    tripSeatingChart: trip.tripSeatingChart
                })
                router.replace(`/trips/${params.tripID}/`)
                router.refresh()
            }}/>
        </div>
    );
};

export default EditVehiclePage;