'use client'

import Heading from "@/components/ui/heading/Heading";
import styles from "../page.module.scss";
import {useEffect, useState} from "react";
import {TripService} from "@/services/trip.service";
import Button from "@/components/ui/button/Button";
import Navbar from "@/components/ui/navbar/Navbar";
import {emptyTrip} from "@/entities/trip";
import {useRouter} from "next/navigation";
import PlacePicker from "@/components/ui/place-picker/PlacePicker";

const EditDestinationPage = ({params}: {params: {tripID: string}}) => {
    const [trip, setTrip] = useState(emptyTrip)

    useEffect(() => {
        TripService.getById(params.tripID).then((res) => setTrip(res))
    }, [])

    const router = useRouter()

    return (
        <>
            <Navbar title={''} showBackButton={true}/>
            <Heading className={styles.heading} text={'Куда едем?'}/>
            <PlacePicker
                value={trip.to}
                onChange={(val) => setTrip({...trip, to: val})}
            />
            <Button
                className={styles.button}
                text={'Готово'}
                onClick={() => {
                    TripService.putById(params.tripID, trip)
                    router.replace(`/trips/${params.tripID}/`)
                    router.refresh()
                }}
            />
        </>
    );
};

export default EditDestinationPage