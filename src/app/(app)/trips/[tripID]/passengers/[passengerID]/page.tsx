'use client';

import Heading from "@/components/ui/heading/Heading";
import Caption from "@/components/ui/caption/Caption";
import styles from "./page.module.scss"
import Button from "@/components/ui/button/Button";
import {TripService} from "@/services/trip.service";
import {Trip} from "@/entities/trip";
import {useEffect, useState} from "react";
import {useRouter} from "next/navigation";
import Navbar from "@/components/ui/navbar/Navbar";
import {emptyTrip} from "@/entities/trip";
import {Passenger} from "@/entities/passenger";

const PassengerDetailPage = ({params}: {params: {tripID: string, passengerID: string}}) => {
    const router = useRouter()

    const [trip, setTrip] = useState<Trip>(emptyTrip)

    useEffect(() => {
        const fetchTrip = async () => {
            const trip = await TripService.getById(params.tripID)
            setTrip(trip)
        }

        fetchTrip()
    })

    const passenger = [...trip.confirmedPassengers.map(v => v.passenger), ...trip.requestPassengers.map(v => v.passenger)].find((passenger) => {return passenger.passengerID == params.passengerID})

    const isReservationConfirmed = (passenger: Passenger | undefined) => {
        return !!trip.confirmedPassengers.find((val) => val.passenger.passengerID == passenger?.passengerID);
    }

    // const toggleReservation = async () => {
    //     if (trip == undefined || passenger == undefined) { return }
    //
    //     const passengerIdx = trip.passengers.indexOf(passenger)
    //     const newTrip = trip
    //
    //     newTrip.passengers[passengerIdx].isReservationAccepted = !newTrip.passengers[passengerIdx].isReservationAccepted;
    //
    //     await TripService.putById(trip.id, newTrip)
    //     router.refresh()
    // }

    return (
        <div>
            <Navbar title={''} showBackButton={true}/>
            <Heading className={styles.heading} text={'Пассажир'}/>
            <div className={styles.infoList}>
                <div className={styles.item}>
                    <Caption captionText={'Имя'}/>
                    <p>{passenger?.firstName}</p>
                </div>
                <div className={styles.item}>
                    <Caption captionText={'Номер телефона'}/>
                    <p>{passenger?.phone}</p>
                </div>
                <div className={styles.item}>
                    <Caption captionText={'Статус'}/>
                    <p className={trip.confirmedPassengers.find((val) => val.passenger.passengerID == passenger?.passengerID) ? styles.textGreen : ''}>
                        { trip.confirmedPassengers.find((val) => val.passenger.passengerID == passenger?.passengerID) ?
                                'Бронирование подтверждено' :
                                'Бронирование не подтверждено' }
                    </p>
                </div>
            </div>
            <div className={styles.buttonList}>
                <Button text={'Связаться с пассажиром'} onClick={() => {

                }}/>
                <Button
                    text={isReservationConfirmed(passenger) ? 'Отменить бронь' : 'Подтвердить бронь'}
                    className={isReservationConfirmed(passenger) ? styles.buttonRed : ''}
                    onClick={() => {
                        // toggleReservation()
                    }}
                />
            </div>
        </div>
    );
};

export default PassengerDetailPage;