'use client'

import Heading from "@/components/ui/heading/Heading";
import styles from "./page.module.scss"
import Image from "next/image";
import RegularTextWithIcon from "@/components/ui/regulat-text-with-icon/RegularTextWithIcon";
import {TripService} from "@/services/trip.service/trip.service";
import DeleteButton from "./_components/DeleteButton";
import EditButton from "./_components/EditButton";
import Link from "next/link";
import Navbar from "@/components/ui/navbar/Navbar";
import dayjs from "dayjs";
import {useEffect, useState} from "react";
import {emptyTrip} from "@/entities/trip";

const TripDetailPage = ({params}: {params: {tripID: string}}) => {
    const [trip, setTrip] = useState(emptyTrip)

    useEffect(() => {
        TripService.getByID(params.tripID).then(trip => {
            if (trip) {
                setTrip(trip)
            }
        })
    }, []);

    return (
        <div>
            <Navbar title={''} showBackButton={true}/>
            <Heading className={styles.heading} text={dayjs(trip.startTime).format('DD MMMM dddd')}/>
            <div className={styles.route}>
                <Heading text={trip.route.startPlace.name}/>
                <Image className={styles.arrow} src={'/decoration/arrow-right.svg'} alt={''} width={27} height={15}/>
                <Heading text={trip.route.endPlace.name}/>
                <div className={styles.time1}>{dayjs(trip.startTime).format('HH:mm')}</div>
                <div className={styles.time2}>{dayjs(trip.endTime).format('HH:mm')}</div>
            </div>
            {
                trip.route.stops ?
                    <>
                        <Heading className={styles.stopListHeading} text={'Остановки по пути'}/>
                        <div className={styles.stopList}>
                            {
                                trip.route.stops?.map((stop) => (
                                    <div className={styles.stop} key={stop.placeID}>
                                        <p>{stop.name}</p>
                                        <p>12:30</p>
                                    </div>
                                ))
                            }
                        </div>
                    </>
                :
                <Heading className={styles.stopListHeading} text={'Без остановок по пути'}/>
            }
            <hr/>
            <div className={styles.infoList}>
                <RegularTextWithIcon text={dayjs(trip.startTime).diff(dayjs(trip.endTime), 'hours') + ' ч в пути'} iconSrc={'/icons/clock.svg'}/>
                <RegularTextWithIcon text={trip.car.mark + ' ' + trip.car.model} iconSrc={'/icons/car.svg'}/>
                <RegularTextWithIcon text={'Перевозчик ' + trip.company.name} iconSrc={'/icons/sheet.svg'}/>
            </div>
            <hr/>
            <div className={styles.seatsInfo}>
                <p>{3}/{5} места свободно</p>
                <div className={styles.price}>
                    <p className={styles.text}>Одно место</p>
                    <p>{1000}</p>
                </div>
            </div>
            <hr/>
            <Heading className={styles.passengersHeading} text={'Пассажиры'}/>
            <div className={styles.passengerList}>
                {/*{*/}
                {/*    trip.confirmedPassengers.map((passenger, idx, passengers) => (*/}
                {/*        <div key={passenger.passenger.passengerID}>*/}
                {/*            <Link href={`/trips/${trip.tripID}/passengers/${passenger.passenger.passengerID}`}>*/}
                {/*                <div className={styles.passenger}>*/}
                {/*                    <p className={styles.textGreen}>{passenger.passenger.firstName}</p>*/}
                {/*                    <Image src={'/icons/chevron-right-sm.svg'} alt={''} width={5} height={10}/>*/}
                {/*                </div>*/}
                {/*            </Link>*/}
                {/*            {idx + 1 < passengers.length ? <hr/> : <div></div>}*/}
                {/*        </div>*/}
                {/*    ))*/}
                {/*}*/}
                {/*{*/}
                {/*    trip.requestPassengers.map((passenger, idx, passengers) => (*/}
                {/*        <div key={passenger.passenger.passengerID}>*/}
                {/*            <Link href={`/trips/${trip.tripID}/passengers/${passenger.passenger.passengerID}`}>*/}
                {/*                <div className={styles.passenger}>*/}
                {/*                    <p className={styles.textGrey}>{passenger.passenger.firstName}</p>*/}
                {/*                    <Image src={'/icons/chevron-right-sm.svg'} alt={''} width={5} height={10}/>*/}
                {/*                </div>*/}
                {/*            </Link>*/}
                {/*            {idx + 1 < passengers.length ? <hr/> : <div></div>}*/}
                {/*        </div>*/}
                {/*    ))*/}
                {/*}*/}
            </div>
            <div className={styles.buttons}>
                <EditButton id={trip.tripID}/>
                <DeleteButton id={trip.tripID}/>
            </div>
        </div>
    );
};

export default TripDetailPage;