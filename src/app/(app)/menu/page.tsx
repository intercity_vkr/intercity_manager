'use client'

import React from 'react';
import Navbar from "@/components/ui/navbar/Navbar";
import Heading from "@/components/ui/heading/Heading";
import {useCookies} from "next-client-cookies";
import {useRouter} from "next/navigation";
import Button from "@/components/ui/button/Button";

const MenuPage = () => {
    const cookies = useCookies()
    const router = useRouter()

    const logout = () => {
        cookies.remove('access_token')
        router.replace('/login')
    }

    return (
        <div>
            <Navbar title={'Меню'} showBackButton={false}/>
            <Button className={'mt-6'} text={'Выйти'} onClick={() => {
                logout()
            }}/>
        </div>
    );
};

export default MenuPage;