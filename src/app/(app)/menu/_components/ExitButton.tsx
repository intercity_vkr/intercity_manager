'use client'

import React from 'react';
import Button from "@/components/ui/button/Button";

const ExitButton = () => {
    return (
        <Button className={'mt-6'} text={'Выйти'} onClick={() => {
            console.log('Выход')
        }}/>
    );
};

export default ExitButton;