import React from 'react';
import Navbar from "@/components/ui/navbar/Navbar";
import Heading from "@/components/ui/heading/Heading";
import styles from "./page.module.scss"
import Link from "next/link";

const ReservationsPage = () => {
    return (
        <div>
            <Navbar title={'Бронирования'}/>
            <Heading text={'Сегодня, 8 февраля, четверг'}/>
            <Link href={'/trips/1/'}>
                <div className={styles.reservationItem}>
                    <div className={styles.left}>
                        <p>Егор забронировал место</p>
                        <p className={'text-grey4'}>Екатеринбург (8:00) – Пионерский (17:00)</p>
                        <p className={'text-grey4'}>8 февраля, пятница</p>
                    </div>
                    <p className={styles.right}>12:01</p>
                </div>
            </Link>
            <hr/>
            <div className={styles.reservationItem}>
                <div className={styles.left}>
                    <p>Егор забронировал место</p>
                    <p className={'text-grey4'}>Екатеринбург (8:00) – Пионерский (17:00)</p>
                    <p className={'text-grey4'}>8 февраля, пятница</p>
                </div>
                <p className={styles.right}>12:01</p>
            </div>
            <hr/>
            <div className={styles.reservationItem}>
                <div className={styles.left}>
                    <p>Егор забронировал место</p>
                    <p className={'text-grey4'}>Екатеринбург (8:00) – Пионерский (17:00)</p>
                    <p className={'text-grey4'}>8 февраля, пятница</p>
                </div>
                <p className={styles.right}>12:01</p>
            </div>
        </div>
    );
};

export default ReservationsPage;