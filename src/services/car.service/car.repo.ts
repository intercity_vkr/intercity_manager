import {CarEntity, CreateCarEntity, UpdateCarEntity} from "@/entities/car";

interface ICarRepo {
    getAll(token: string): Promise<CarEntity[]>;
    getById(token: string, carID: string): Promise<CarEntity|undefined>;
    create(token: string, createCarEntity: CreateCarEntity): Promise<CarEntity>;
    update(token: string, carID: string, updateCarEntity: UpdateCarEntity): Promise<CarEntity>;
}

export default ICarRepo;