import {CarEntity} from '@/entities/car';
import axios, { AxiosResponse } from 'axios';
import {axiosConfigWithAuthHeader} from "@/services/common";
import ICarRepo from "@/services/car.service/car.repo";

class CarService {
    carRepo: ICarRepo

    constructor(carRepo: ICarRepo) {
        this.carRepo = carRepo;
    }

    async getAll(): Promise<CarEntity[]> {
        const accessTokenCookie = require('next/headers').cookies().get('access_token')
        return accessTokenCookie ? this.carRepo.getAll(accessTokenCookie.value) : []
    }

    // async getById(carID: string): Promise<CarEntity|undefined> {
    //     try {
    //         const response: AxiosResponse = await axios.get(`${API_URL}/list`, axiosConfigWithAuthHeader());
    //         return response.data.map((apiCar: any) => fromAPICar(apiCar)).find((car: CarEntity) => car.carID == carID);
    //     } catch (error) {
    //         console.error(`Error while fetching car with ID ${carID}:`, error);
    //         throw error;
    //     }
    // }
    //
    // async create(carData: CreateCarInput): Promise<CarEntity> {
    //     try {
    //         const response: AxiosResponse<CarEntity> = await axios.post(`${API_URL}/create`, toAPICreateCarRequest(carData), axiosConfigWithAuthHeader());
    //         return fromAPICar(response.data);
    //     } catch (error) {
    //         console.error('Error while creating car:', error);
    //         throw error;
    //     }
    // }
    //
    // async update(carID: string, carData: CreateCarInput): Promise<CarEntity> {
    //     try {
    //         const response: AxiosResponse<CarEntity> = await axios.put(`${API_URL}/update`, {...toAPICreateCarRequest(carData), car_id: carID}, axiosConfigWithAuthHeader());
    //         return response.data;
    //     } catch (error) {
    //         console.error(`Error while updating car with ID ${carID}:`, error);
    //         throw error;
    //     }
    // }
}
