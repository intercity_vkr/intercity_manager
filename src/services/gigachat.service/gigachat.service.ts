import axios, {isAxiosError} from "axios";
import Cookies from "universal-cookie";

const getGigachatAccessTokenFromAPI = async (): Promise<string|null> => {
    let data = JSON.stringify({
        'scope': 'GIGACHAT_API_PERS'
    });
    let config = {
        method: 'post',
        maxBodyLength: Infinity,
        url: 'https://ngw.devices.sberbank.ru:9443/api/v2/oauth',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Authorization': 'Basic MDg2Y2Y0NDUtMDNiMC00NTViLTg1MzEtMzNiNDQ5MTFiNmM2OjkxOWQ1NzVjLTI5MTUtNGM4NC1hZTA2LWNiZGIzOTY4YzBmMA=='
        },
        data : data
    };

    axios(config)
        .then((response) => {
            console.log(JSON.stringify(response.data));
            const cookies = new Cookies(null, { path: '/' })
            cookies.set('gigachat_access_token', response.data.access_token)
            return response.data.access_token;
        })
        .catch((error) => {
            console.log(error);
        });
    return null
}

const getGigachatAccessTokenFromCookies = (): string | null => {
    const cookies = new Cookies(null, { path: '/' })
    const access_token = cookies.get('gigachat_access_token')
    if (!access_token) {
        return null
    }
    return access_token
}

const getGigachatAccessToken = async(): Promise<string|null> => {
    const token = getGigachatAccessTokenFromCookies()
    if (!token) {
        const token = await getGigachatAccessTokenFromAPI()
        if (token) {
            return token
        }
        return null
    }
    return token
}

export const GigachatService = {
    async getCityDescription (city: string): Promise<string|null> {
        const token = getGigachatAccessToken()

        let data = JSON.stringify({
            "model": "GigaChat",
            "messages": [
                {
                    "role": "user",
                    "content": `Расскажи вкратце о городе ${city}! Какие там есть достопримечательности? Что стоит посетить?`
                }
            ],
            "temperature": 1,
            "top_p": 0.1,
            "n": 1,
            "stream": false,
            "max_tokens": 512,
            "repetition_penalty": 1,
            "update_interval": 0
        });

        let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: 'https://gigachat.devices.sberbank.ru/api/v1/chat/completions',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            data : data
        };

        axios(config)
            .then((response) => {
                console.log(JSON.stringify(response.data));
                return response.data.choices[0].message.content
            })
            .catch((error) => {
                if (isAxiosError(error)) {
                    if (error.status == 401) {
                        getGigachatAccessTokenFromAPI()
                    }
                }
                console.log(error);
            });
        return null
    }
}