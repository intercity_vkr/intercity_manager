import axios from "axios";
import appConfig from "@/app-config";
import {Vehicle} from "@/entities/vehicle";

const apiUrl = appConfig.jsonServerUrl + '/vehicles'

export const VehicleService = {
    async getAll(): Promise<Vehicle[]> {
        const response = await axios.get(apiUrl)
        return response.data
    },

    async getById(id: string): Promise<Vehicle> {
        const response = await axios.get(apiUrl + '/' + id)
        return response.data
    },

    async deleteById(id: string): Promise<Vehicle> {
        const response = await axios.delete(apiUrl + '/' + id)
        return response.data
    },

    async putById(id: string, vehicle: Vehicle): Promise<Vehicle> {
        const response = await axios.put(apiUrl + '/' + id, vehicle)
        return response.data
    },

    async create(vehicle: Vehicle): Promise<string> {
        const {id: _, ...vehicleData} = vehicle
        const response = await axios.post(apiUrl, vehicleData)
        return response.data
    },

}