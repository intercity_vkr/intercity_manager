import {CreateTripInput, fromAPITrip, toAPICreateTripRequest, Trip} from "@/entities/trip";
import axios, {AxiosResponse} from "axios";
import appConfig from "@/app-config";
import {axiosConfigWithAuthHeader} from "@/services/common";

const API_URL = `${appConfig.apiUrl}/trip`

export const TripService = {
    async getAll(): Promise<Trip[]> {
        try {
            const response: AxiosResponse<Trip[]> = await axios.get(`${API_URL}/list`, axiosConfigWithAuthHeader());
            return response.data.map((apiTrip) => fromAPITrip(apiTrip));
        } catch (error) {
            console.error('Error while fetching trips:', error);
            return []
        }
    },

    async getByID(tripID: string): Promise<Trip|undefined> {
        try {
            const response: AxiosResponse<Trip[]> = await axios.get(`${API_URL}/list`, axiosConfigWithAuthHeader());
            return response.data.map((apiTrip: any) => fromAPITrip(apiTrip)).find((trip) => trip.tripID == tripID);
        } catch (error) {
            console.error('Error while fetching trips:', error);
            throw error;
        }
    },

    async create(tripData: CreateTripInput): Promise<Trip> {
        try {
            const response: AxiosResponse<Trip> = await axios.post(`${API_URL}/create`, toAPICreateTripRequest(tripData), axiosConfigWithAuthHeader());
            return fromAPITrip(response.data);
        } catch (error) {
            console.error('Error while creating trip:', error);
            throw error;
        }
    },

    async update(tripID: string, tripData: CreateTripInput): Promise<Trip> {
        try {
            const response: AxiosResponse<Trip> = await axios.put(`${API_URL}/update`, {...toAPICreateTripRequest(tripData), trip_id: tripID}, axiosConfigWithAuthHeader());
            return fromAPITrip(response.data);
        } catch (error) {
            console.error('Error while creating trip:', error);
            throw error;
        }
    },
};