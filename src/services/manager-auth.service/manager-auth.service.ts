import AppConfig from "@/app-config";
import IManagerAuthRepo from "@/services/manager-auth.service/manager-auth.repo";
const cookies = require('next/headers').cookies

const apiUrl = AppConfig.apiUrl + '/manager/auth/'

export class ManagerAuthService {
    managerAuthRepo: IManagerAuthRepo

    constructor(managerAuthRepo: IManagerAuthRepo) {
        this.managerAuthRepo = managerAuthRepo;
    }

    async initAuth(phone: string) {
        const initAuthResult = await this.managerAuthRepo.initAuth(phone)
        cookies().set('manager_id', initAuthResult.managerID, {httpOnly: true})
    }

    async code(code: string) {
        const managerID = require('next/headers').cookies().get('manager_id')
        if (!managerID) { return }
        const codeAuthResult = await this.managerAuthRepo.code(managerID.value, code)
        cookies().set('access_token', codeAuthResult.accessToken, {httpOnly: true})
        cookies().set('refresh_token', codeAuthResult.refreshToken, {httpOnly: true})
    }

}