import {ManagerAuthCodeResponseEntity, ManagerAuthInitResponseEntity} from "@/entities/manager-auth";

interface IManagerAuthRepo {
    initAuth(phone: string): Promise<ManagerAuthInitResponseEntity>;
    code(managerID: string, code: string): Promise<ManagerAuthCodeResponseEntity>;
}

export default IManagerAuthRepo;