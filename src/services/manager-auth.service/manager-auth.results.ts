enum EInitAuthResult { Success, UserNotFound, UnknownError }

enum ECodeResult { Success, NoManagerID, WrongCode, UnknownError }

export { EInitAuthResult, ECodeResult }