import axios, {AxiosResponse} from "axios";
import appConfig from "@/app-config";
import {Route, RouteFilterRequest, toAPICreateRouteRequest} from "@/entities/route";
import {CreateRouteInput, fromAPIRoute} from "@/entities/route";
import {axiosConfigWithAuthHeader} from "@/services/common";

const API_URL = `${appConfig.apiUrl}/route`

export const RouteService = {
    async getAll(): Promise<Route[]> {
        try {
            const response: AxiosResponse<Route[]> = await axios.get(`${API_URL}/list`, axiosConfigWithAuthHeader());
            return response.data.map((route) => fromAPIRoute(route));
        } catch (error) {
            console.error('Error while fetching routes:', error);
            throw error;
        }
    },

    async create(routeData: CreateRouteInput): Promise<Route> {
        try {
            const response: AxiosResponse<Route> = await axios.post(`${API_URL}/create`, toAPICreateRouteRequest(routeData), axiosConfigWithAuthHeader());
            return fromAPIRoute(response.data);
        } catch (error) {
            console.error('Error while creating route:', error);
            throw error;
        }
    },

    async filter(filterData: RouteFilterRequest): Promise<Route[]> {
        try {
            const response: AxiosResponse<any[]> = await axios.post(`${API_URL}/filter`, filterData, axiosConfigWithAuthHeader());
            return response.data.map((apiRoute) => fromAPIRoute(apiRoute));
        } catch (error) {
            console.error('Error while filtering routes:', error);
            throw error;
        }
    },
};