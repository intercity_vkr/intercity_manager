import Cookies from "universal-cookie";
import {AxiosRequestConfig} from "axios";

const getTokenFromCookies = (): string | null => {
    const cookies = new Cookies(null, { path: '/' })
    const access_token = cookies.get('access_token')
    if (access_token) {
        return access_token
    }
    return null
}

const axiosConfigWithAuthHeader = (): AxiosRequestConfig => {
    const token = getTokenFromCookies()
    return  {headers: {Authorization: 'Bearer ' + (token ?? '')}}
}

export {getTokenFromCookies, axiosConfigWithAuthHeader}