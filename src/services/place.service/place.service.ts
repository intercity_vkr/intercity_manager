import {CreatePlaceInput, fromAPIPlace, Place, toAPICreatePlaceRequest} from "@/entities/place";
import axios, {AxiosResponse} from "axios";
import appConfig from "@/app-config";
import {axiosConfigWithAuthHeader} from "@/services/common";

const API_URL = `${appConfig.apiUrl}/place`

export const PlaceService = {
    async getAll(): Promise<Place[]> {
        try {
            const response: AxiosResponse<Place[]> = await axios.get(`${API_URL}/list`, axiosConfigWithAuthHeader());
            return response.data.map((apiPlace) => fromAPIPlace(apiPlace));
        } catch (error) {
            console.error('Error while fetching places:', error);
            throw error;
        }
    },

    async create(placeData: CreatePlaceInput): Promise<Place> {
        try {
            const response: AxiosResponse<Place> = await axios.post(`${API_URL}/create`, toAPICreatePlaceRequest(placeData), axiosConfigWithAuthHeader());
            return fromAPIPlace(response.data);
        } catch (error) {
            console.error('Error while creating place:', error);
            throw error;
        }
    },
};