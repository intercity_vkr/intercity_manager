import axios from "axios";
import appConfig from "@/app-config";
import {Trip} from "@/entities/trip";
import Cookies from 'universal-cookie';

const apiUrl = appConfig.jsonServerUrl + '/trips'

export const TripService = {
    async getAll(): Promise<Trip[]> {
        const cookies = new Cookies(null, { path: '/' })
        const response = await axios.get(apiUrl + '/')
        return response.data
    },

    async getById(id: string): Promise<Trip> {
        const response = await axios.get(apiUrl + '/' + id)
        return response.data
    },

    async deleteById(id: string): Promise<Trip> {
        const response = await axios.delete(apiUrl + '/' + id)
        return response.data
    },

    async putById(id: string, trip: Trip): Promise<Trip> {
        const response = await axios.put(apiUrl + '/' + id, trip)
        return response.data
    },

    async create(trip: Trip): Promise<string> {
        const {tripID: _, ...tripData} = trip
        const response = await axios.post(apiUrl, tripData)
        return response.data
    }

}