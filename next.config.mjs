/** @type {import('next').NextConfig} */
const nextConfig = {
    async rewrites() {

        return [
            {
                source: '/api/gigachat/v1/:path*',
                destination: 'https://ngw.devices.sberbank.ru:9443'
            },
            {
                source: '/api/gigachat/v2/:path*',
                destination: 'https://gigachat.devices.sberbank.ru'
            }
        ]
    }
};

export default nextConfig;
