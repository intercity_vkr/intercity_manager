# Intercity – Front-end

"Межгород" – агрегатор междугородних пассажирских перевозок.

Предназначен для работы в связке с [Intercity Backend](https://gitlab.com/intercity_vkr/intercity_backend/).

## Установка зависимостей
### [Next.js](https://nextjs.org/)
```bash
npm install next@latest react@latest react-dom@latest
```

### Библиотеки
```bash
npm install
```

## Настройка API
Данное веб-приложение предназначено для работы в связке с [Intercity Backend](https://gitlab.com/intercity_vkr/intercity_backend/). Сначала разверните Intercity Backend по инструкции из README.md.

Далее в файле **@/app-config.ts** укажите путь к API.
```typescript
const appConfig = {
    apiUrl: 'http://localhost:4343'
}
```



## Запуск
### Dev
```bash
npm run dev
```

### Prod
```bash
npm build
npm start
```

## Как выглядит приложение
![1.png](screenshots%2F1.png)
![2.png](screenshots%2F2.png)
![3.png](screenshots%2F3.png)
![4.png](screenshots%2F4.png)
